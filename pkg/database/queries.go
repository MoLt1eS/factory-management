package database

import (
	"context"
	"factory-management/pkg/domain"
	"log"
	"strconv"
	"time"
)

// FetchProductList Fetches Product list from the database
func FetchProductList() []domain.Product {

	resultList := []domain.Product{}

	rows, err := DB.Query("SELECT s.StorageID, s.Name FROM Storages s")

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {

		var p domain.Product

		err := rows.Scan(&p.ID, &p.Name)
		if err != nil {
			log.Fatal(err)
		}
		resultList = append(resultList, p)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return resultList
}

// FetchStorageList fetches all storage from the database
func FetchStorageList() []domain.Storage {

	storageList := []domain.Storage{}

	rows, err := DB.Query("SELECT s.StorageID, s.Name FROM Storages s")

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {

		var s domain.Storage

		err := rows.Scan(&s.ID, &s.Name)
		if err != nil {
			log.Fatal(err)
		}

		s.Compartments = FetchStorageCompartments(s.ID)

		storageList = append(storageList, s)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return storageList
}

// FetchStorageCompartments fetches all Compartments associated to a given storage
func FetchStorageCompartments(storageID string) []domain.Compartment {

	compartmentList := []domain.Compartment{}

	rows, err := DB.Query("SELECT c.CompartmentID, c.Name, c.VisualCoordinate FROM Compartments c JOIN Storages s ON c.StorageID == s.StorageID AND s.StorageID == ?", storageID)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {

		var compartment domain.Compartment

		err := rows.Scan(&compartment.ID, &compartment.Name, &compartment.VisualCoordinate)
		if err != nil {
			log.Fatal(err)
		}
		compartmentList = append(compartmentList, compartment)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return compartmentList
}

// FetchEntrysGivenCompartment fetches all entrys from a given compartment
func FetchEntrysGivenCompartment(compartmentID string) []domain.Entry {
	compartmentList := []domain.Entry{}

	rows, err := DB.Query("SELECT e.EntryID, e.ProductID, e.CompartmentID, e.Used, e.EntryDate FROM Entrys e WHERE e.CompartmentID == ?", compartmentID)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	for rows.Next() {

		var entry domain.Entry

		err := rows.Scan(&entry.ID, &entry.ProductID, &entry.CompartmentID, &entry.Used, &entry.EntryDate)
		if err != nil {
			log.Fatal(err)
		}

		compartmentList = append(compartmentList, entry)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return compartmentList
}

// InsertNewEntry inserts a new given entry transaction require to deal with Entry.Used on existing entrys on this compartment
func InsertNewEntry(entry *domain.Entry) {

	// Generate date if not set, this is due to avoiding an extra SELECT to fill the object
	// Plus for this use case we don't case about the database timezone as this is for local usage :) .Format(time.RFC3339)
	if entry.EntryDate.IsZero() {
		entry.EntryDate = time.Now()
	}

	ctx := context.Background()

	tx, err := DB.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	_, err = tx.ExecContext(ctx, "UPDATE Entrys SET Used = true WHERE CompartmentID = ?", entry.CompartmentID)
	if err != nil {
		log.Fatal(err)
		tx.Rollback()
	}

	stmt, err := tx.Prepare("INSERT INTO Entrys (ProductID, CompartmentID, EntryDate) VALUES(?, ?, ?)")
	if err != nil {
		log.Fatal(err)
		tx.Rollback()
	}

	defer stmt.Close()

	res, err := stmt.Exec(entry.ProductID, entry.CompartmentID, entry.EntryDate.Format(time.RFC3339))
	if err != nil {
		log.Fatal(err)
		tx.Rollback()
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
		tx.Rollback()
	}

	// Commit.
	tx.Commit()

	entry.ID = strconv.FormatInt(lastID, 10)
}

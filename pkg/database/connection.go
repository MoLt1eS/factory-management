package database

import (
	"database/sql"
	"io/ioutil"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

// DatabaseName default database name
// Why I started with this simple approach
// https://www.alexedwards.net/blog/organising-database-access
const DatabaseName string = "./factory-database.db"

// DB Create an exported global variable to hold the database connection pool.
var DB *sql.DB

// This will always iniciate when calling eny methong on this "class"
func init() {
	var err error
	// Initalize the sql.DB connection pool and assign it to the models.DB
	// global variable.
	DB, err = sql.Open("sqlite3", DatabaseName) // Open the created SQLite File
	if err != nil {
		log.Fatal(err)
	}
}

// StartDatabase starts database instance
func StartDatabase() {

	// Checks if the database already exists
	log.Println("Booting database...")

	rows, err := DB.Query("select * from Products;")

	if err == nil {
		log.Println("Database already exists, skipping creating database")
		rows.Close()
		return
	}

	log.Println("Creating database...")
	file, err := ioutil.ReadFile("./scripts/database/factory-management.sql")
	if err != nil {
		log.Println(err.Error())
	}

	_, err = DB.Exec(string(file))
	if err != nil {
		log.Println(err.Error())
	}

	log.Println("Finished setting up database!")
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

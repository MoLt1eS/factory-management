package domain

import "time"

// Compartment represents a compartment data structure
type Compartment struct {
	ID               string `json:"id"`
	StorageID        string `json:"storage_id"`
	VisualCoordinate string `json:"visual_coordinate"`
	Name             string `json:"name"`
}

// Product represents a product data structure
type Product struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Storage represents a storage data structure
type Storage struct {
	ID           string        `json:"id"`
	Name         string        `json:"name"`
	Compartments []Compartment `json:"compartments"`
}

// Entry represents an entry data structure
type Entry struct {
	ID            string    `json:"id"`
	ProductID     string    `json:"product_id"`
	CompartmentID string    `json:"compartment_id"`
	Used          bool      `json:"used"`
	EntryDate     time.Time `json:"entry_time"`
}

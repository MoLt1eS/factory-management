package server

import (
	"encoding/json"
	"factory-management/pkg/database"
	"factory-management/pkg/domain"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

// fetchProducts fetches list of all products
func fetchProducts(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(database.FetchProductList())
}

// fetchStorages fetches list of all storages and compartments associated to each storage
func fetchStorages(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(database.FetchStorageList())
}

// createEntry create a Product Entry
func createEntry(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := ioutil.ReadAll(r.Body)

	var newEntry domain.Entry

	json.Unmarshal(reqBody, &newEntry)

	database.InsertNewEntry(&newEntry)

	json.NewEncoder(w).Encode(newEntry)

}

// fetchEntrysInCompartment deals with getting all Entrys in a given compartment
func fetchEntrysInCompartment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	compartmentID := vars["compartmentID"]

	entryList := database.FetchEntrysGivenCompartment(compartmentID)

	json.NewEncoder(w).Encode(entryList)

}

package server

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Start inicializes the http server
func Start() {
	r := mux.NewRouter()
	r.HandleFunc("/api/products", fetchProducts)
	r.HandleFunc("/api/storages", fetchStorages)
	r.HandleFunc("/api/compartment/{compartmentID}", fetchEntrysInCompartment)
	r.HandleFunc("/api/entry", createEntry).Methods("POST")
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	log.Println("Server started.")
	http.ListenAndServe(":3000", r)
}

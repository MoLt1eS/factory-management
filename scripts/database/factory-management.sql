-- DROP TABLE IF EXISTS Entrys;
-- DROP TABLE IF EXISTS Compartments; 
-- DROP TABLE IF EXISTS Storages; 
-- DROP TABLE IF EXISTS Products; 

PRAGMA foreign_keys = ON;

-- Informacao dos produtos
CREATE TABLE Products (
	ProductID INTEGER PRIMARY KEY AUTOINCREMENT,
	Name text not null
);

-- Informacao de armazem
CREATE TABLE Storages (
	StorageID INTEGER PRIMARY KEY AUTOINCREMENT,
	Name text not null
);

-- Informacao dos compartimentos associados a um dado armazem
CREATE TABLE Compartments (
	CompartmentID INTEGER PRIMARY KEY AUTOINCREMENT,
	Name text not null,
	StorageID not null,
	VisualCoordinate text,
	FOREIGN KEY (StorageID) REFERENCES Storages(StorageID)
);
-- Entradas de novos produtos
CREATE TABLE Entrys (
	EntryID INTEGER PRIMARY KEY AUTOINCREMENT,
	ProductID integer not null,
	CompartmentID integer not null,
	Used INTEGER default(false),
	EntryDate datetime default CURRENT_TIMESTAMP,
	FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
	FOREIGN KEY (CompartmentID) REFERENCES Compartments(CompartmentID)
);

-- Criacao dos armazens
INSERT INTO Storages (
	Name 
) VALUES
	("A"),
	("B"),
	("C");

-- Criacao dos compartimentos de cada armazen
INSERT INTO Compartments (
	Name,
	StorageID,
	VisualCoordinate 
) VALUES 
	("A1", (SELECT StorageID FROM Storages WHERE Name == "A"), "3,1"),
	("A2", (SELECT StorageID FROM Storages WHERE Name == "A"), "2,1"),
	("A3", (SELECT StorageID FROM Storages WHERE Name == "A"), "1,1"),
	("A4", (SELECT StorageID FROM Storages WHERE Name == "A"), "1,2"),
	("A5", (SELECT StorageID FROM Storages WHERE Name == "A"), "1,3"),
	("A6", (SELECT StorageID FROM Storages WHERE Name == "A"), "2,3"),
	("A7", (SELECT StorageID FROM Storages WHERE Name == "A"), "3,3"),
	
	("B1", (SELECT StorageID FROM Storages WHERE Name == "B"), "3,1"),
	("B2", (SELECT StorageID FROM Storages WHERE Name == "B"), "2,1"),
	("B3", (SELECT StorageID FROM Storages WHERE Name == "B"), "1,1"),
	("B4", (SELECT StorageID FROM Storages WHERE Name == "B"), "1,2"),
	("B5", (SELECT StorageID FROM Storages WHERE Name == "B"), "1,3"),
	("B6", (SELECT StorageID FROM Storages WHERE Name == "B"), "2,3"),
	("B7", (SELECT StorageID FROM Storages WHERE Name == "B"), "3,3"),
	
	("C1", (SELECT StorageID FROM Storages WHERE Name == "C"), "3,1"),
	("C2", (SELECT StorageID FROM Storages WHERE Name == "C"), "2,1"),
	("C3", (SELECT StorageID FROM Storages WHERE Name == "C"), "1,1"),
	("C4", (SELECT StorageID FROM Storages WHERE Name == "C"), "1,2"),
	("C5", (SELECT StorageID FROM Storages WHERE Name == "C"), "1,3"),
	("C6", (SELECT StorageID FROM Storages WHERE Name == "C"), "2,3"),
	("C7", (SELECT StorageID FROM Storages WHERE Name == "C"), "3,3");

-- Lista de produtos
INSERT INTO Products (
	Name 
) VALUES 
	("Milho"),
	("Sêmea de Arroz"),
	("Soja Integral"),
	("Sêmea de Trigo"),
	("Luzerna Desidratada"),
	("Bagaço de Girassol"),
	("Farinha Amarela"),
	("Cevada"),
	("Aveia"),
	("Alfarroba Fina"),
	("Alfarroba Triturada Grada"),
	("Bagaço de Colza"),
	("Polpa de Beterraba"),
	("Palha Granulada"),
	("Misturas"),
	("Extrusados");
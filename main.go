package main

import (
	"factory-management/pkg/database"
	"factory-management/pkg/server"
)

func main() {

	database.StartDatabase()
	server.Start()

}
